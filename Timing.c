//#include "TROOT.h"
//#include "TFile.h"
//#include "TTree.h"
//#include "TBrowser.h"
//#include "TH2.h"
//#include "TRandom.h"



void Timing()
{
    
    TPaveText *pt = new TPaveText(.05,.1,.95,.8);
 ///   gStyle->SetErrorX(0);
  //  gPad->SetTicky();
 //   gStyle->SetOptStat(0);
//    gStyle->SetOptTitle(0);
    TGaxis::SetMaxDigits(4);
    gStyle->SetHatchesSpacing(1.5);
    gStyle->SetHatchesLineWidth(1);
    TPad* p1 = new TPad("p1","p1",0.05,0.3,0.95,0.95,0); p1->Draw();
    TLegend *legend=new TLegend(0.6,0.55,0.95,0.9);
    legend->SetBorderSize(0);
    legend->SetTextSize(0.035);
    TLegend *legend2=new TLegend(0.4,0.45,0.75,0.8);
    legend2->SetBorderSize(0);
    legend2->SetTextSize(0.035);
    
    TLegend *legend4=new TLegend(0.6,0.6,0.95,0.95);
    legend4->SetBorderSize(0);
    legend4->SetTextSize(0.035);
    
    TLegend *legend5=new TLegend(0.55,0.6,0.75,0.95);
    legend5->SetBorderSize(0);
    legend5->SetTextSize(0.035);
    
    TLegend *legend7=new TLegend(0.55,0.6,0.75,0.95);
    legend7->SetBorderSize(0);
    legend7->SetTextSize(0.035);
    
    TLegend *legend6=new TLegend(0.55,0.6,0.75,0.95);
    legend6->SetBorderSize(0);
    legend6->SetTextSize(0.035);
    
    TLegend *legend3=new TLegend(0.6,0.45,0.95,0.8);
    legend3->SetBorderSize(0);
    legend3->SetTextSize(0.035);
    p1->cd();
    
    char nl[26];
    char nl2[26];
    std::vector<float> * RecHits = 0;
    std::vector<float> * Time = 0;
    std::vector<float> * Charge = 0;
    std::vector<float> * Charge0 = 0;
    std::vector<float> * Charge1 = 0;
    std::vector<float> * Charge2 = 0;
    std::vector<float> * Charge3 = 0;
    std::vector<float> * Charge4 = 0;
    std::vector<float> * Charge5 = 0;
    std::vector<float> * Charge6 = 0;
    std::vector<float> * Charge7 = 0;
    
    std::vector<float> * TDC0 = 0;
    std::vector<float> * TDC1 = 0;
    std::vector<float> * TDC2 = 0;
    std::vector<float> * TDC3 = 0;
    std::vector<float> * TDC4 = 0;
    std::vector<float> * TDC5 = 0;
    std::vector<float> * TDC6 = 0;
    std::vector<float> * TDC7 = 0;
 
    bool trig1 ;
    bool trig2 ;
    bool trig3 ;
    bool trig4 ;
    
    
    std::vector<int> * iEta = 0;
    
    std::vector<int> * iPhi = 0;
    
    std::vector<int> * iDepth = 0;
    
    int colors[8] = {1, 2, 3, 4, 41, 6, 7, 28};
    
//TFile *f = new TFile("analysisTreeExo.root");
//TFile *f = new TFile("analysisTreeBs3.root");
//    TFile *f = new TFile("default-filename_352425_3.root");
  
    TChain* chain = new TChain("wztree");
 
    
 //   chain->Add("default-filename_320500.root");
    //chain->Add("default-filename_352425.root");
//    chain->Add("default-filename_355133.root");
    
 /*
    chain->Add("default-filename_352425_1.root");
    chain->Add("default-filename_352425_2.root");
    chain->Add("default-filename_352425_3.root");
    chain->Add("default-filename_352425_4.root");
    chain->Add("default-filename_352425_5.root");
   */
   /*
    chain->Add("./356077/output_*.root");
    chain->Add("./356076/output_*.root");
    chain->Add("./356075/output_*.root");
    chain->Add("./356074/output_*.root");
    chain->Add("./356071/output_*.root");
     */
//    chain->Add("./2018IB/default-filename_2018ib.root");
    chain->Add("./320995/default-filename.root");
 //   chain->Add("./zerobias/output_*.root");
 //   chain->Add("./zerobias_2/output_*.root");
 //   chain->Add("./zerobias_3/output3_*.root");
  //  chain->Add("./zerobias_6/output6_*.root");
//    chain->Add("./MC2/results.root");
//TTree *t1 = (TTree*)f->Get("wztree");
    TTree* t1 =   chain;
    
    
 t1->SetBranchAddress("RecHit",&RecHits);
    t1->SetBranchAddress("iEta",&iEta);
    t1->SetBranchAddress("iPhi",&iPhi);
    t1->SetBranchAddress("iDepth",&iDepth);
    t1->SetBranchAddress("Time",&Time);
    t1->SetBranchAddress("Charge",&Charge);
    t1->SetBranchAddress("Charge0",&Charge0);
    t1->SetBranchAddress("Charge1",&Charge1);
    t1->SetBranchAddress("Charge2",&Charge2);
    t1->SetBranchAddress("Charge3",&Charge3);
    t1->SetBranchAddress("Charge4",&Charge4);
    t1->SetBranchAddress("Charge5",&Charge5);
    t1->SetBranchAddress("Charge6",&Charge6);
    t1->SetBranchAddress("Charge7",&Charge7);
    
    t1->SetBranchAddress("trig1",&trig1);
    t1->SetBranchAddress("trig2",&trig2);
    t1->SetBranchAddress("trig3",&trig3);
    t1->SetBranchAddress("trig4",&trig4);

    
    t1->SetBranchAddress("TDC0",&TDC0);
     t1->SetBranchAddress("TDC1",&TDC1);
     t1->SetBranchAddress("TDC2",&TDC2);
     t1->SetBranchAddress("TDC3",&TDC3);
     t1->SetBranchAddress("TDC4",&TDC4);
     t1->SetBranchAddress("TDC5",&TDC5);
     t1->SetBranchAddress("TDC6",&TDC6);
     t1->SetBranchAddress("TDC7",&TDC7);
    
    TH1F *hRecHitTimePhi[8][72];
    TH1F *hTDCPhi[8][72];
    TH1F *hTDCEta[8][26];
    
    TH1F *hCWPhi[8][72];
    TH1F *hCWEta[8][26];
    
    int nHitsPhi[8][72];
    
    TH1F *hRecHitTimeEta[8][26];
    TH1F *hRecHitTimeEtaPhi[8][26][72];
    TH1F *hTDCTimeEtaPhi[8][26][72];
    TH2F *hTDCChargeTimeEtaPhi[8][26][72];
    TH1F *hCWTimeEtaPhi[8][26][72];
    
    double x[72];
    double y[72];
    double ex[72];
    double ey[72];
    
    for(int i = 0; i < 8; i++)
    {
        for(int j = 0; j < 72; j++)
        {
            sprintf(nl,"HE_idepth_%d_iphi_%d", i, j);
            hRecHitTimePhi[i][j] = new TH1F(nl,nl,400,-20,20);
            
            sprintf(nl,"TDC_HE_idepth_%d_iphi_%d", i, j);
            hTDCPhi[i][j] = new TH1F(nl,nl,50,0,25);
            
            sprintf(nl,"CW_HE_idepth_%d_iphi_%d", i, j);
            hCWPhi[i][j] = new TH1F(nl,nl,170,0,170);
            for(int k = 0; k< 26; k++)
            {
                sprintf(nl,"HE_%d_%d_%d", i, k, j);
               hRecHitTimeEtaPhi[i][k][j] = new TH1F(nl,nl,400,-20,20);
                sprintf(nl,"TDCTime_%d_%d_%d", i, k, j);

                hTDCTimeEtaPhi[i][k][j] = new TH1F(nl,nl,50,0,25);

                sprintf(nl,"TDCQTime_%d_%d_%d", i, k, j);
                hTDCChargeTimeEtaPhi[i][k][j] = new TH2F(nl,nl,1000,0,10000, 50,0,25);
                
                sprintf(nl,"CWTime_%d_%d_%d", i, k, j);
                hCWTimeEtaPhi[i][k][j] = new TH1F(nl,nl,170,0,170);
            }
        }
    }
    
    for(int i = 0; i < 8; i++)
    {
        for(int j = 0; j < 26; j++)
        {
            sprintf(nl,"TDC_HE_idepth_%d_ieta_%d", i, j);
            hTDCEta[i][j] = new TH1F(nl,nl,50,0,25);
            sprintf(nl,"CW_HE_idepth_%d_ieta_%d", i, j);
            hCWEta[i][j] = new TH1F(nl,nl,170,0,170);
            sprintf(nl,"HE_idepth_%d_ieta_%d", i, j);
            hRecHitTimeEta[i][j] = new TH1F(nl,nl,400,-20,20);
        }
    }
    
    TH1F *hChargeFraction   = new TH1F("hChargeFraction","hChargeFraction",100,0,1.0);
    
    TH1F *hNTDCTS   = new TH1F("hNTDCTS","hNTDCTS",9,0,9);
    TH1F *hTriggs   = new TH1F("hTriggs","hTriggs",4,0,4);
    TH1F *hRecHitE   = new TH1F("hRecHitE","hRecHitE",1100,-1,10);
    TH1F *hRecHitTime   = new TH1F("hRecHitTime","hRecHitTime",400,-20,20);
    TH1F *hRecHitTimeAllChannels   = new TH1F("hRecHitTimellChannels","hRecHitTimellChannels",200,-10,10);
    TH1F *hRecHitCATime   = new TH1F("hRecHitCATime","hRecHitCATime",160,10,170);
    TH1F *hRecHitCATimeAllChannels   = new TH1F("hRecHitCATimeAllChannels","hRecHitCATimeAllChannels",160,10,170);
    TH1F *hTDCTimeAllChannels   = new TH1F("hTDCTimeAllChannels","hTDCTimeAllChannels",50,0,25);
    TH1F *hRecHitTimeMax   = new TH1F("hRecHitTimeMax","hRecHitTimeMax",1200,-11000,1000);
    TH1F *hRecHitQ   = new TH1F("hRecHiQ","hRecHitQ",1000,0,10000);
    TH2F *hRecHitTimeQ   = new TH2F("hRecHitTimeQ","hRecHitTimeQ",1000,0,10000, 200,-5,15);
    
    TH2F *TDCoutliersHeatMapTDC1   = new TH2F("TDCoutliersHeatMapTDC1","TDCoutliersHeatMapTDC1",27,0,27,72,0,72);
    TH2F *TDCoutliersHeatMapTDC2   = new TH2F("TDCoutliersHeatMapTDC2","TDCoutliersHeatMapTDC2",27,0,27,72,0,72);
    TH2F *TDCoutliersHeatMapTDC3   = new TH2F("TDCoutliersHeatMapTDC3","TDCoutliersHeatMapTDC3",27,0,27,72,0,72);
    TH2F *TDCoutliersHeatMapTDC4   = new TH2F("TDCoutliersHeatMapTDC4","TDCoutliersHeatMapTDC4",27,0,27,72,0,72);
    TH2F *TDCoutliersHeatMapTDC5   = new TH2F("TDCoutliersHeatMapTDC5","TDCoutliersHeatMapTDC5",27,0,27,72,0,72);
    TH2F *TDCoutliersHeatMapTDC6   = new TH2F("TDCoutliersHeatMapTDC6","TDCoutliersHeatMapTDC6",27,0,27,72,0,72);
    TH2F *TDCoutliersHeatMapTDC7   = new TH2F("TDCoutliersHeatMapTDC7","TDCoutliersHeatMapTDC7",27,0,27,72,0,72);
    
    TH2F *TDCoutliersHeatMapCWT1   = new TH2F("TDCoutliersHeatMapCWT1","TDCoutliersHeatMapCWT1",27,0,27,72,0,72);
        TH2F *TDCoutliersHeatMapCWT2   = new TH2F("TDCoutliersHeatMapCWT2","TDCoutliersHeatMapCWT2",27,0,27,72,0,72);
        TH2F *TDCoutliersHeatMapCWT3   = new TH2F("TDCoutliersHeatMapCWT3","TDCoutliersHeatMapCWT3",27,0,27,72,0,72);
        TH2F *TDCoutliersHeatMapCWT4   = new TH2F("TDCoutliersHeatMapCWT4","TDCoutliersHeatMapCWT4",27,0,27,72,0,72);
        TH2F *TDCoutliersHeatMapCWT5   = new TH2F("TDCoutliersHeatMapCWT5","TDCoutliersHeatMapCWT5",27,0,27,72,0,72);
        TH2F *TDCoutliersHeatMapCWT6   = new TH2F("TDCoutliersHeatMapCWT6","TDCoutliersHeatMapCWT6",27,0,27,72,0,72);
        TH2F *TDCoutliersHeatMapCWT7   = new TH2F("TDCoutliersHeatMapCWT7","TDCoutliersHeatMapCWT7",27,0,27,72,0,72);
    
    TH2F *CWToutliersHeatMapTDC1   = new TH2F("CWToutliersHeatMapTDC1","CWToutliersHeatMapTDC1",27,0,27,72,0,72);
        TH2F *CWToutliersHeatMapTDC2   = new TH2F("CWToutliersHeatMapTDC2","CWToutliersHeatMapTDC2",27,0,27,72,0,72);
        TH2F *CWToutliersHeatMapTDC3   = new TH2F("CWToutliersHeatMapTDC3","CWToutliersHeatMapTDC3",27,0,27,72,0,72);
        TH2F *CWToutliersHeatMapTDC4   = new TH2F("CWToutliersHeatMapTDC4","CWToutliersHeatMapTDC4",27,0,27,72,0,72);
        TH2F *CWToutliersHeatMapTDC5   = new TH2F("CWToutliersHeatMapTDC5","CWToutliersHeatMapTDC5",27,0,27,72,0,72);
        TH2F *CWToutliersHeatMapTDC6   = new TH2F("CWToutliersHeatMapTDC6","CWToutliersHeatMapTDC6",27,0,27,72,0,72);
        TH2F *CWToutliersHeatMapTDC7   = new TH2F("CWToutliersHeatMapTDC7","CWToutliersHeatMapTDC7",27,0,27,72,0,72);
        
        TH2F *CWToutliersHeatMapCWT1   = new TH2F("CWToutliersHeatMapCWT1","CWToutliersHeatMapCWT1",27,0,27,72,0,72);
            TH2F *CWToutliersHeatMapCWT2   = new TH2F("CWToutliersHeatMapCWT2","CWToutliersHeatMapCWT2",27,0,27,72,0,72);
            TH2F *CWToutliersHeatMapCWT3   = new TH2F("CWToutliersHeatMapCWT3","CWToutliersHeatMapCWT3",27,0,27,72,0,72);
            TH2F *CWToutliersHeatMapCWT4   = new TH2F("CWToutliersHeatMapCWT4","CWToutliersHeatMapCWT4",27,0,27,72,0,72);
            TH2F *CWToutliersHeatMapCWT5   = new TH2F("CWToutliersHeatMapCWT5","CWToutliersHeatMapCWT5",27,0,27,72,0,72);
            TH2F *CWToutliersHeatMapCWT6   = new TH2F("CWToutliersHeatMapCWT6","CWToutliersHeatMapCWT6",27,0,27,72,0,72);
            TH2F *CWToutliersHeatMapCWT7   = new TH2F("CWToutliersHeatMapCWT7","CWToutliersHeatMapCWT7",27,0,27,72,0,72);
    
    TH2F *BothoutliersHeatMapTDC1   = new TH2F("BothoutliersHeatMapTDC1","BothoutliersHeatMapTDC1",27,0,27,72,0,72);
        TH2F *BothoutliersHeatMapTDC2   = new TH2F("BothoutliersHeatMapTDC2","BothoutliersHeatMapTDC2",27,0,27,72,0,72);
        TH2F *BothoutliersHeatMapTDC3   = new TH2F("BothoutliersHeatMapTDC3","BothoutliersHeatMapTDC3",27,0,27,72,0,72);
        TH2F *BothoutliersHeatMapTDC4   = new TH2F("BothoutliersHeatMapTDC4","BothoutliersHeatMapTDC4",27,0,27,72,0,72);
        TH2F *BothoutliersHeatMapTDC5   = new TH2F("BothoutliersHeatMapTDC5","BothoutliersHeatMapTDC5",27,0,27,72,0,72);
        TH2F *BothoutliersHeatMapTDC6   = new TH2F("BothoutliersHeatMapTDC6","BothoutliersHeatMapTDC6",27,0,27,72,0,72);
        TH2F *BothoutliersHeatMapTDC7   = new TH2F("BothoutliersHeatMapTDC7","BothoutliersHeatMapTDC7",27,0,27,72,0,72);
        
        TH2F *BothoutliersHeatMapCWT1   = new TH2F("BothoutliersHeatMapCWT1","BothoutliersHeatMapCWT1",27,0,27,72,0,72);
            TH2F *BothoutliersHeatMapCWT2   = new TH2F("BothoutliersHeatMapCWT2","BothoutliersHeatMapCWT2",27,0,27,72,0,72);
            TH2F *BothoutliersHeatMapCWT3   = new TH2F("BothoutliersHeatMapCWT3","BothoutliersHeatMapCWT3",27,0,27,72,0,72);
            TH2F *BothoutliersHeatMapCWT4   = new TH2F("BothoutliersHeatMapCWT4","BothoutliersHeatMapCWT4",27,0,27,72,0,72);
            TH2F *BothoutliersHeatMapCWT5   = new TH2F("BothoutliersHeatMapCWT5","BothoutliersHeatMapCWT5",27,0,27,72,0,72);
            TH2F *BothoutliersHeatMapCWT6   = new TH2F("BothoutliersHeatMapCWT6","BothoutliersHeatMapCWT6",27,0,27,72,0,72);
            TH2F *BothoutliersHeatMapCWT7   = new TH2F("BothoutliersHeatMapCWT7","BothoutliersHeatMapCWT7",27,0,27,72,0,72);
    
    
    TH2F *hCWTimeQ   = new TH2F("hCWTimeQ","hCWTimeQ",1000,0,10000, 160,10,170);
    TH2F *hCWTimeQ2   = new TH2F("hCWTimeQ2","hCWTimeQ2",5000,0,50000, 170,0,170);

   // TH2F *hRecHitTimeQ   = new TH2F("hRecHitTimeQ","hRecHitTimeQ",1000,0,10000, 160,10,170);
    TH2F *hRecHitEnergyQ   = new TH2F("hRecHitEnergyQ","hRecHitEnergyQ",1000,0,10000, 1100,-1,10);
    
    TH2F *hRecHitQTDC   = new TH2F("hRecHitQTDC","hRecHitQTDC",1000,0,10000, 50, 0.0, 25.0);
    
    TH2F *hRecHiTimeEnergy   = new TH2F("hRecHiTimeEnergy","hRecHiTimeEnergy",200,-5,15, 1100,-1,10);
    TH2F *hRecHitTime12   = new TH2F("hRecHitTimeQ","hRecHitTimeQ",200,-5,15, 160,10,170);
    Long64_t nentries1 = t1->GetEntries();
    cout<<"nentries:  "<<nentries1<<endl;
    
    int n = 100000;

    int counter = 0;
        for (Long64_t i=0;i<nentries1;i++) {
          t1->GetEntry(i);
           
       //     double  w = (*GenWeight)[0] > 0 ? 1.0 : -1.0 ;
            if(trig1 == true) hTriggs->Fill(0);
            if(trig2 == true) hTriggs->Fill(1);
            if(trig3 == true) hTriggs->Fill(2);
            if(trig4 == true) hTriggs->Fill(3);
           
         //   cout<<trig1<<" | "<<trig2<<" | "<<trig3<<" | "<<trig4<<" | "<<(trig1 + trig2 + trig3 + trig4)<<endl;
            for(int k = 0; k<RecHits->size(); k++)
            {
                if(fabs((*iEta)[k]) < 17 ) continue;
          //      if((*iEta)[k] > -17 ) continue;
          //      if((*iEta)[k] < 17 ) continue;
              //  cout<<"old vs new:  "<<(*RecHits)[k]<<" , "<<(*RecHitsNew)[k]<<endl;
              //  if((*Time)[k] < -999) continue;
               // if((*RecHits)[k] < 1) continue;
            //    if((*iEta)[k] < -29 || (*iEta)[k] > -17) continue;
            //   if((*iDepth)[k] != 5) continue;
           //     if((*iEta)[k] != 20) continue;
            //    if((*RecHits)[k] > 0.5) continue;
        //     if((*RecHits)[k] > 0.25) continue;
      
       //         if((*iEta)[k] != 26 ) continue;
       //         if((*iDepth)[k] != 1 ) continue;
           //     if((*iPhi)[k] != 19 ) continue;
                
            // if(trig3 == true)   hNTDCTS->Fill(((*TDC0)[k] > 0) + ((*TDC1)[k] > 0) + ((*TDC2)[k] > 0) + ((*TDC3)[k] > 0) + ((*TDC4)[k] > 0) + ((*TDC5)[k] > 0) + ((*TDC6)[k] > 0) + ((*TDC7)[k] > 0) );
                
                
                double averageTime = ( 25*(*Charge1)[k] + 50*(*Charge2)[k] + 75*(*Charge3)[k] + 100*(*Charge4)[k] + 125*(*Charge5)[k] + 150*(*Charge6)[k] + 175*(*Charge7)[k])/((*Charge0)[k] + (*Charge1)[k] + (*Charge2)[k] + (*Charge3)[k] + (*Charge4)[k] + (*Charge5)[k] + (*Charge6)[k] + (*Charge7)[k]) ;
             //   if((*Charge)[k] < 5000.0) continue;
                

                if(trig3 !=  true) continue;
                hCWTimeQ->Fill((*Charge)[k], averageTime);
                hCWTimeQ2->Fill((*Charge)[k], averageTime);
                if( (*Charge)[k] > 2000) hChargeFraction->Fill((*Charge3)[k]/(*Charge)[k]);

                if((*Charge3)[k] > 0 && (*TDC3)[k] >= 0)
                {
                hRecHitQTDC->Fill((*Charge3)[k], (*TDC3)[k]);
                }
                
           //     if((*iPhi)[k] < 3 ||  (*iPhi)[k] > 26 ) continue;
                hRecHitEnergyQ->Fill((*Charge)[k], (*RecHits)[k]);
                hRecHiTimeEnergy->Fill((*Time)[k], (*RecHits)[k]);
                hRecHitE->Fill((*RecHits)[k]);
                hRecHitTime->Fill((*Time)[k]);
                hRecHitQ->Fill((*Charge)[k]);
                hRecHitTimeMax->Fill((*Time)[k]);
               // cout<<"depth:  "<<(*iDepth)[k]<<endl;
                hRecHitTimePhi[(*iDepth)[k]][(*iPhi)[k]]->Fill((*Time)[k]);
                
                if((*TDC3)[k] > 0 && (*Charge3)[k] > 2000 ) hTDCPhi[(*iDepth)[k]][(*iPhi)[k]]->Fill((*TDC3)[k]);
                if( (*Charge)[k] > 2000 ) hCWPhi[(*iDepth)[k]][(*iPhi)[k]]->Fill(averageTime);

              
                if((*iEta)[k] < 0)
                {
                    hRecHitTimeEta[(*iDepth)[k]][29+(*iEta)[k]]->Fill((*Time)[k]);
                    if((*Charge3)[k] > 2000)  hRecHitTimeEtaPhi[(*iDepth)[k]][29+(*iEta)[k]][(*iPhi)[k]]->Fill((*Time)[k]);
                    if((*Charge)[k] > 2000) hCWTimeEtaPhi[(*iDepth)[k]][29+(*iEta)[k]][(*iPhi)[k]]->Fill(averageTime);
                    
                    if((*TDC3)[k] > 0 && (*Charge3)[k] > 2000)
                    {
                        hTDCTimeEtaPhi[(*iDepth)[k]][29+(*iEta)[k]][(*iPhi)[k]]->Fill((*TDC3)[k]);
                        hTDCChargeTimeEtaPhi[(*iDepth)[k]][29+(*iEta)[k]][(*iPhi)[k]]->Fill((*Charge3)[k], (*TDC3)[k]);
                    }
                    if((*TDC3)[k] > 0 && (*Charge3)[k] > 2000) hTDCEta[(*iDepth)[k]][29+(*iEta)[k]]->Fill((*TDC3)[k]);
                    
                    if( (*Charge)[k] > 2000) hCWEta[(*iDepth)[k]][29+(*iEta)[k]]->Fill(averageTime);
                }
                else
                {
                    hRecHitTimeEta[(*iDepth)[k]][(*iEta)[k]-4]->Fill((*Time)[k]);
                    if((*Charge3)[k] > 2000) hRecHitTimeEtaPhi[(*iDepth)[k]][(*iEta)[k]-4][(*iPhi)[k]]->Fill((*Time)[k]);
                    if((*Charge)[k] > 2000) hCWTimeEtaPhi[(*iDepth)[k]][(*iEta)[k]-4][(*iPhi)[k]]->Fill(averageTime);
                    if((*TDC3)[k] > 0 && (*Charge3)[k] > 2000) hTDCEta[(*iDepth)[k]][(*iEta)[k]-4]->Fill((*TDC3)[k]);
                    if( (*Charge)[k] > 2000) hCWEta[(*iDepth)[k]][(*iEta)[k]-4]->Fill(averageTime);

                    if( (*TDC3)[k] > 0 && (*Charge3)[k] > 2000)
                    {
                        hTDCTimeEtaPhi[(*iDepth)[k]][(*iEta)[k]-4][(*iPhi)[k]]->Fill((*TDC3)[k]);
                        hTDCChargeTimeEtaPhi[(*iDepth)[k]][(*iEta)[k]-4][(*iPhi)[k]]->Fill((*Charge3)[k], (*TDC3)[k]);
                    }

               //     cout<<"here"<<endl;
                }
                
              //  double averageTime = (25*(*Charge1)[k] + 50*(*Charge2)[k] + 75*(*Charge3)[k] 100*(*Charge4)[k])/((*Charge1)[k] + (*Charge2)[k] + (*Charge3)[k] + (*Charge4)[k]);
              //  double averageTime = (25*(*Charge0)[k] + 50*(*Charge1)[k] + 75*(*Charge2)[k] + 100*(*Charge3)[k] + 125*(*Charge4)[k] + 150*(*Charge5)[k] + 175*(*Charge6)[k] + 200*(*Charge7)[k])/((*Charge0)[k] + (*Charge1)[k] + (*Charge2)[k] + (*Charge3)[k] + (*Charge4)[k] + (*Charge5)[k] + (*Charge6)[k] + (*Charge7)[k]) ;
               
               
                hRecHitCATime->Fill(averageTime);
                // cout<<"q1:  "<<(*Charge)[k]<<" q2: "<<((*Charge0)[k] + (*Charge1)[k] + (*Charge2)[k] + (*Charge3)[k] + (*Charge4)[k] + (*Charge5)[k] + (*Charge6)[k] + (*Charge7)[k])<<endl;
            //.    cout<<"average time:  "<<averageTime<<endl;
                hRecHitTimeQ->Fill((*Charge3)[k], (*Time)[k]);
                //hRecHitTimeQ->Fill((*Charge)[k], averageTime);
                hRecHitTime12->Fill((*Time)[k], averageTime);
           //     x[counter] = (*Charge)[k];
           //     y[counter] =(*Time)[k];
           //     counter++;
            //    cout<<"t, E "<<(*Time)[k]<<" , "<<(*RecHits)[k]<<endl;
              
            }
        }
    hRecHitTime->Draw();
    cout<<nentries1<<endl;
    TGraph *gr = new TGraph(counter,x,y);
    gr->SetMarkerColor(kBlack);
    gr->SetMarkerStyle(8);
   // hRecHitE->Draw("HIST");
 //   gr->Draw("AP");
    hRecHitTime->Draw();
  //  hRecHitQ->Draw();
   // hRecHitE->Draw("HIST");
//    hRecHitEnergyQ->Draw("COLZ");
  //  hRecHiTimeEnergy->Draw("COLZ");
    hRecHiTimeEnergy->GetXaxis()->SetTitle("M2 Time");
    hRecHiTimeEnergy->GetYaxis()->SetTitle("Energy");
    
    hRecHitTimeQ->GetYaxis()->SetTitle("M2 Time");
    hRecHitTimeQ->GetXaxis()->SetTitle("Q");
    
    hRecHitCATime->Draw();
    
    hRecHitQTDC->GetYaxis()->SetTitle("TDC time");
    hRecHitQTDC->GetXaxis()->SetTitle("Charge");
    
 
 
    for(int i = 0; i < 8; i ++)
    {
        for(int j = 0; j< 26; j++)
        {
            for(int k = 0; k<72; k++)
            {
               // if(i != 1 || k != 3) continue;
             //   if(i != 7 ) continue;
             //   cout<<"separate entries n: "<<hTDCTimeEtaPhi[i][j][k]->GetEntries()<<endl;
                
              //  if(hTDCTimeEtaPhi[i][j][k]->GetEntries() > 0 )
              //  {
                 //   cout<<"i, j, k:  "<<i<<" , "<<j<<" , "<<k<<" N "<<hCWTimeEtaPhi[i][j][k]->GetMean()<<endl;
              //  hRecHitTimeAllChannels->Fill(hRecHitTimeEtaPhi[i][j][k]->GetMean());
                    
                    int ietaBin = j+1;
                    if(j > 12) ietaBin = j+2;
                    double diff = fabs(6.865 - hTDCTimeEtaPhi[i][j][k]->GetMean());
                    if(hTDCTimeEtaPhi[i][j][k]->GetEntries() < 1) diff = -1;
                    if(i == 1)  TDCoutliersHeatMapTDC1->SetBinContent(ietaBin, k, diff);
                    if(i == 2)  TDCoutliersHeatMapTDC2->SetBinContent(ietaBin, k, diff);
                    if(i == 3)  TDCoutliersHeatMapTDC3->SetBinContent(ietaBin, k, diff);
                    if(i == 4)  TDCoutliersHeatMapTDC4->SetBinContent(ietaBin, k, diff);
                    if(i == 5)  TDCoutliersHeatMapTDC5->SetBinContent(ietaBin, k, diff);
                    if(i == 6)  TDCoutliersHeatMapTDC6->SetBinContent(ietaBin, k, diff);
                    if(i == 7)  TDCoutliersHeatMapTDC7->SetBinContent(ietaBin, k, diff);
                    
                    if(ietaBin == 14) cout<<"!!!!!!!!!!!"<<endl;
                if(hTDCTimeEtaPhi[i][j][k]->GetEntries() > 0 )
                 {
                     hTDCTimeAllChannels->Fill(hTDCTimeEtaPhi[i][j][k]->GetMean());
                 }
                    if(fabs(6.865 - hTDCTimeEtaPhi[i][j][k]->GetMean()) > 2.0 )
                    {
                        int ieta = -29 + j;
                        if(j > 12) ieta = 4 + j;
                    //    cout<<"depth, ieta, iphi, mean TDC, n entries:  "<<i<<" | "<<ieta<<" | "<<k<<" | "<<hTDCTimeEtaPhi[i][j][k]->GetMean()<<" | "<<hTDCTimeEtaPhi[i][j][k]->GetEntries()<<endl;
                      //  cout<<"depth, ieta, iphi, mean CWT, mean TDC, CWT entries, TDC entries:  "<<i<<" | "<<ieta<<" | "<<k<<" | "<<" | "<<hCWTimeEtaPhi[i][j][k]->GetMean()<<" | "<<hTDCTimeEtaPhi[i][j][k]->GetMean()<<" | "<<hCWTimeEtaPhi[i][j][k]->GetEntries()<<" | "<<hTDCTimeEtaPhi[i][j][k]->GetEntries()<<endl;
                       
                    
                       /*
                        hCWTimeEtaPhi[i][j][k]->Draw();
                        sprintf(nl,"CWT_%d_%d_%d.pdf", ieta, k, i);
                        hCWTimeEtaPhi[i][j][k]->SetTitle(nl);
                        sprintf(nl,"/Users/anton/HCAL/TDCoutliers/CWT_%d_%d_%d.pdf", ieta, k, i);
                        p1->SaveAs(nl);
                        */
                        
                    }
                    
                     
            //    }
                
                
                if(fabs(6.865 - hTDCTimeEtaPhi[i][j][k]->GetMean()) > 2.0 && hTDCTimeEtaPhi[i][j][k]->GetEntries() > 0 && hCWTimeEtaPhi[i][j][k]->GetEntries() > 0 && fabs(92.91 - hCWTimeEtaPhi[i][j][k]->GetMean()) > 2.5)
                {
                    int ieta = -29 + j;
                    if(j > 12) ieta = 4 + j;
                  //  cout<<"depth, ieta, iphi, mean TDC, mean CWT:  "<<i<<" | "<<ieta<<" | "<<k<<" | "<<hTDCTimeEtaPhi[i][j][k]->GetMean()<<" | "<<hCWTimeEtaPhi[i][j][k]->GetMean()<<endl;
                    /*
                    hTDCTimeEtaPhi[i][j][k]->Draw();
                    sprintf(nl,"TDC_%d_%d_%d.pdf", ieta, k, i);
                    hTDCTimeEtaPhi[i][j][k]->SetTitle(nl);
                    sprintf(nl,"/Users/anton/HCAL/Bothoutliers/TDC_%d_%d_%d.pdf", ieta, k, i);
                    p1->SaveAs(nl);
                //   p1->SaveAs(nl);
                    
                    hCWTimeEtaPhi[i][j][k]->Draw();
                    sprintf(nl,"CWT_%d_%d_%d.pdf", ieta, k, i);
                    hCWTimeEtaPhi[i][j][k]->SetTitle(nl);
                    sprintf(nl,"/Users/anton/HCAL/Bothoutliers/CWT_%d_%d_%d.pdf", ieta, k, i);
                    p1->SaveAs(nl);
                     */
                }
                 
                
                
                if(hCWTimeEtaPhi[i][j][k]->GetEntries() > 0 )
                {
                    hRecHitCATimeAllChannels->Fill(hCWTimeEtaPhi[i][j][k]->GetMean());
                }
                     ietaBin = j+1;
                                       if(j > 12) ietaBin = j+2;
                                        diff = fabs(92.91 - hCWTimeEtaPhi[i][j][k]->GetMean());
                                       if(hCWTimeEtaPhi[i][j][k]->GetEntries() < 1) diff = -1;
                                       if(i == 1)  CWToutliersHeatMapCWT1->SetBinContent(ietaBin, k, diff);
                                       if(i == 2)  CWToutliersHeatMapCWT2->SetBinContent(ietaBin, k, diff);
                                       if(i == 3)  CWToutliersHeatMapCWT3->SetBinContent(ietaBin, k, diff);
                                       if(i == 4)  CWToutliersHeatMapCWT4->SetBinContent(ietaBin, k, diff);
                                       if(i == 5)  CWToutliersHeatMapCWT5->SetBinContent(ietaBin, k, diff);
                                       if(i == 6)  CWToutliersHeatMapCWT6->SetBinContent(ietaBin, k, diff);
                                       if(i == 7)  CWToutliersHeatMapCWT7->SetBinContent(ietaBin, k, diff);
                    
                    if(fabs(92.91 - hCWTimeEtaPhi[i][j][k]->GetMean()) > 2.5)
                    {
                        int ieta = -29 + j;
                        if(j > 12) ieta = 4 + j;
                      //  cout<<"depth, ieta, iphi, mean CWT, mean TDC, CWT entries, TDC entries:  "<<i<<" | "<<ieta<<" | "<<k<<" | "<<" | "<<hCWTimeEtaPhi[i][j][k]->GetMean()<<" | "<<hTDCTimeEtaPhi[i][j][k]->GetMean()<<" | "<<hCWTimeEtaPhi[i][j][k]->GetEntries()<<" | "<<hTDCTimeEtaPhi[i][j][k]->GetEntries()<<endl;
                     /*
                        sprintf(nl,"CWT_ieta_%d_iphi_%d_depth_%d.pdf", ieta, k, i);
                        hCWTimeEtaPhi[i][j][k]->SetTitle(nl);
                        hCWTimeEtaPhi[i][j][k]->Draw();
                        sprintf(nl,"/Users/anton/HCAL/CWToutliers/CWT_ieta_%d_iphi_%d_depth_%d.pdf", ieta, k, i);
                        p1->SaveAs(nl);
                        hTDCTimeEtaPhi[i][j][k]->Draw();
                        sprintf(nl,"TDC_ieta_%d_iphi_%d_depth_%d.pdf", ieta, k, i);
                        hTDCTimeEtaPhi[i][j][k]->SetTitle(nl);
                        sprintf(nl,"/Users/anton/HCAL/CWToutliers/TDC_ieta_%d_iphi_%d_depth_%d.pdf", ieta, k, i);
                        p1->SaveAs(nl);
                      */
                      
                    //    p1->SaveAs(nl);
                    }
                    
                
            
                
                 
             //   if(hRecHitTimeEtaPhi[i][j][k]->GetEntries() > 0)
             //   {
             //       hTDCTimeAllChannels->Fill(hRecHitTimeEtaPhi[i][j][k]->GetMean());
             //   }
                
            }
        }
    }
 
 //   hRecHitTimeAllChannels->Draw();
 //   hRecHitCATimeAllChannels->Draw();
  //  hCWTimeQ->Draw("COLZ");
    
    TDCoutliersHeatMapTDC1->GetXaxis()->SetTitle("i#eta");
    TDCoutliersHeatMapTDC1->GetYaxis()->SetTitle("i#phi");
    TDCoutliersHeatMapTDC2->GetXaxis()->SetTitle("i#eta");
    TDCoutliersHeatMapTDC2->GetYaxis()->SetTitle("i#phi");
    TDCoutliersHeatMapTDC3->GetXaxis()->SetTitle("i#eta");
    TDCoutliersHeatMapTDC3->GetYaxis()->SetTitle("i#phi");
    TDCoutliersHeatMapTDC4->GetXaxis()->SetTitle("i#eta");
    TDCoutliersHeatMapTDC4->GetYaxis()->SetTitle("i#phi");
    TDCoutliersHeatMapTDC5->GetXaxis()->SetTitle("i#eta");
    TDCoutliersHeatMapTDC5->GetYaxis()->SetTitle("i#phi");
    TDCoutliersHeatMapTDC6->GetXaxis()->SetTitle("i#eta");
    TDCoutliersHeatMapTDC6->GetYaxis()->SetTitle("i#phi");
    TDCoutliersHeatMapTDC7->GetXaxis()->SetTitle("i#eta");
    TDCoutliersHeatMapTDC7->GetYaxis()->SetTitle("i#phi");
    
    hTDCTimeAllChannels->Draw();
    p1->SaveAs("/Users/anton/HCAL/TDCoutliers/AllChannels18.pdf");
    hRecHitCATimeAllChannels->Draw();
    p1->SaveAs("/Users/anton/HCAL/CWToutliers/AllChannels18.pdf");
//hChargeFraction->Draw();
  //  hCWTimeQ2->Draw("COLZ");
    /*
    TDCoutliersHeatMapTDC1->Draw("COLZ");
    p1->SaveAs("/Users/anton/HCAL/TDCoutliers/TDCHeatMapDepth1.pdf");
    TDCoutliersHeatMapTDC2->Draw("COLZ");
    p1->SaveAs("/Users/anton/HCAL/TDCoutliers/TDCHeatMapDepth2.pdf");
    TDCoutliersHeatMapTDC3->Draw("COLZ");
    p1->SaveAs("/Users/anton/HCAL/TDCoutliers/TDCHeatMapDepth3.pdf");
    TDCoutliersHeatMapTDC4->Draw("COLZ");
    p1->SaveAs("/Users/anton/HCAL/TDCoutliers/TDCHeatMapDepth4.pdf");
    TDCoutliersHeatMapTDC5->Draw("COLZ");
    p1->SaveAs("/Users/anton/HCAL/TDCoutliers/TDCHeatMapDepth5.pdf");
    TDCoutliersHeatMapTDC6->Draw("COLZ");
    p1->SaveAs("/Users/anton/HCAL/TDCoutliers/TDCHeatMapDepth6.pdf");
    TDCoutliersHeatMapTDC7->Draw("COLZ");
    p1->SaveAs("/Users/anton/HCAL/TDCoutliers/TDCHeatMapDepth7.pdf");
    
    
    CWToutliersHeatMapCWT1->GetXaxis()->SetTitle("i#eta");
        CWToutliersHeatMapCWT1->GetYaxis()->SetTitle("i#phi");
        CWToutliersHeatMapCWT2->GetXaxis()->SetTitle("i#eta");
        CWToutliersHeatMapCWT2->GetYaxis()->SetTitle("i#phi");
        CWToutliersHeatMapCWT3->GetXaxis()->SetTitle("i#eta");
        CWToutliersHeatMapCWT3->GetYaxis()->SetTitle("i#phi");
        CWToutliersHeatMapCWT4->GetXaxis()->SetTitle("i#eta");
        CWToutliersHeatMapCWT4->GetYaxis()->SetTitle("i#phi");
        CWToutliersHeatMapCWT5->GetXaxis()->SetTitle("i#eta");
        CWToutliersHeatMapCWT5->GetYaxis()->SetTitle("i#phi");
        CWToutliersHeatMapCWT6->GetXaxis()->SetTitle("i#eta");
        CWToutliersHeatMapCWT6->GetYaxis()->SetTitle("i#phi");
        CWToutliersHeatMapCWT7->GetXaxis()->SetTitle("i#eta");
        CWToutliersHeatMapCWT7->GetYaxis()->SetTitle("i#phi");
        

        CWToutliersHeatMapCWT1->Draw("COLZ");
        p1->SaveAs("/Users/anton/HCAL/CWToutliers/CWTHeatMapDepth1.pdf");
        CWToutliersHeatMapCWT2->Draw("COLZ");
        p1->SaveAs("/Users/anton/HCAL/CWToutliers/CWTHeatMapDepth2.pdf");
        CWToutliersHeatMapCWT3->Draw("COLZ");
        p1->SaveAs("/Users/anton/HCAL/CWToutliers/CWTHeatMapDepth3.pdf");
        CWToutliersHeatMapCWT4->Draw("COLZ");
        p1->SaveAs("/Users/anton/HCAL/CWToutliers/CWTHeatMapDepth4.pdf");
        CWToutliersHeatMapCWT5->Draw("COLZ");
        p1->SaveAs("/Users/anton/HCAL/CWToutliers/CWTHeatMapDepth5.pdf");
        CWToutliersHeatMapCWT6->Draw("COLZ");
        p1->SaveAs("/Users/anton/HCAL/CWToutliers/CWTHeatMapDepth6.pdf");
        CWToutliersHeatMapCWT7->Draw("COLZ");
        p1->SaveAs("/Users/anton/HCAL/CWToutliers/CWTHeatMapDepth7.pdf");
*/
     }

