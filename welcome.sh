#!/bin/bash

echo "welcome to HTCondor tutorial"

pwd

cd /afs/cern.ch/work/a/astepenn/HCAL/CMSSW_12_4_3/src/test/Select
pwd

echo $1

cmsenv
file=$(./testScript $1)
echo "will process"
echo $file
cmsRun ./python/my_config_RAW2DIGI_RECO.py inputFileName=$file outputFileName=./zerobias8/output8_$(($1)).root
