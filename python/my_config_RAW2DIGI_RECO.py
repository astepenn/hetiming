# Auto generated configuration file
# using: 


# Revision: 1.19 
# Source: /local/reps/CMSSW/CMSSW/Configuration/Applications/python/ConfigBuilder.py,v 
# with command line options: my_config --filein --data --eventcontent RECO --conditions 124X_dataRun3_Prompt_v4 --step RAW2DIGI,RECO --nThreads 4 --geometry DB:Extended --era Run3 -n 10 --no_exec
import FWCore.ParameterSet.Config as cms

from FWCore.ParameterSet.VarParsing import VarParsing
import sys
import os

from Configuration.Eras.Era_Run3_cff import Run3

process = cms.Process('RECO',Run3)

options = dict()
varOptions = VarParsing('analysis')
varOptions.register( "inputFileName", "TEST_file.root", VarParsing.multiplicity.singleton, VarParsing.varType.string, "Input file name"  )
varOptions.register( "outputFileName", "results.root", VarParsing.multiplicity.singleton, VarParsing.varType.string, "Output file name"                           )
varOptions.parseArguments()


# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.RawToDigi_Data_cff')
process.load('Configuration.StandardSequences.Reconstruction_Data_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

process.hbhereco.cpu.algorithm.useMahi = cms.bool(False)
process.hbhereco.cpu.algorithm.useM2 = cms.bool(True)
process.hbhereco.cpu.algorithm.useM3 = cms.bool(False)
##process.hbhereco.cpu.algorithm.ts4chi2 = cms.vdouble(9999.,9999.)

#process.TFileService = cms.Service("TFileService", fileName = cms.string("mahidebugger.root") )


process.hbhereco.cpu.saveInfos = cms.bool(True)
#process.load("RecoLocalCalo.HcalRecAlgos.test.mahiDebugger_cfi")

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(-1),
    output = cms.optional.untracked.allowed(cms.int32,cms.PSet)
)


# Input source
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
'file:/eos/cms/store/data/Run2022C/EphemeralHLTPhysics0/RAW/v1/000/356/071/00000/09e7b1f4-8c5b-4668-b808-beb348724bfb.root'
),
    secondaryFileNames = cms.untracked.vstring()
)
process.source.fileNames = [varOptions.inputFileName]

process.options = cms.untracked.PSet(
    FailPath = cms.untracked.vstring(),
    IgnoreCompletely = cms.untracked.vstring(),
    Rethrow = cms.untracked.vstring(),
    SkipEvent = cms.untracked.vstring(),
    accelerators = cms.untracked.vstring('*'),
    allowUnscheduled = cms.obsolete.untracked.bool,
    canDeleteEarly = cms.untracked.vstring(),
    deleteNonConsumedUnscheduledModules = cms.untracked.bool(True),
    dumpOptions = cms.untracked.bool(False),
    emptyRunLumiMode = cms.obsolete.untracked.string,
    eventSetup = cms.untracked.PSet(
        forceNumberOfConcurrentIOVs = cms.untracked.PSet(
            allowAnyLabel_=cms.required.untracked.uint32
        ),
        numberOfConcurrentIOVs = cms.untracked.uint32(0)
    ),
    fileMode = cms.untracked.string('FULLMERGE'),
    forceEventSetupCacheClearOnNewRun = cms.untracked.bool(False),
    makeTriggerResults = cms.obsolete.untracked.bool,
    numberOfConcurrentLuminosityBlocks = cms.untracked.uint32(0),
    numberOfConcurrentRuns = cms.untracked.uint32(1),
    numberOfStreams = cms.untracked.uint32(0),
    numberOfThreads = cms.untracked.uint32(1),
    printDependencies = cms.untracked.bool(False),
    sizeOfStackForThreadsInKB = cms.optional.untracked.uint32,
    throwIfIllegalParameter = cms.untracked.bool(True),
    wantSummary = cms.untracked.bool(False)
)

# Production Info
process.configurationMetadata = cms.untracked.PSet(
    annotation = cms.untracked.string('my_config nevts:10'),
    name = cms.untracked.string('Applications'),
    version = cms.untracked.string('$Revision: 1.19 $')
)

# Output definition

process.RECOoutput = cms.OutputModule("PoolOutputModule",
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string(''),
        filterName = cms.untracked.string('')
    ),
    fileName = cms.untracked.string('my_config_RAW2DIGI_RECO.root'),
    outputCommands = process.RECOEventContent.outputCommands,
    splitLevel = cms.untracked.int32(0)
)

# Additional output definition

# Other statements
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '124X_dataRun3_Prompt_v4', '')

# Path and EndPath definitions
process.raw2digi_step = cms.Path(process.RawToDigi)
#process.reconstruction_step = cms.Path(process.reconstruction)

process.reconstruction_step = cms.Path(process.hbhereco)

process.endjob_step = cms.EndPath(process.endOfProcess)
process.RECOoutput_step = cms.EndPath(process.RECOoutput)

#process.my_analyzer = cms.Process("Demo")
process.analysis = cms.EDAnalyzer('Select',

outputFileName = cms.string(varOptions.outputFileName)
)

process.filter = cms.EDFilter("HLTHighLevel",
    TriggerResultsTag = cms.InputTag("TriggerResults","","HLT"),
    HLTPaths = cms.vstring('HLT_ZeroBias_IsolatedBunches_v6'),           # provide list of HLT paths (or patterns) you want
    eventSetupPathsKey = cms.string(''), # not empty => use read paths from AlCaRecoTriggerBitsRcd via this key
    andOr = cms.bool(False),             # how to deal with multiple triggers: True (OR) accept if ANY is true, False (AND) accept if ALL are true
    throw = cms.bool(True)    # throw exception on unknown path names
)


#process.TFileService = cms.Service("TFileService", fileName = cms.string( varOptions.outputFileName ) )

process.hltFilter_step = cms.Path(process.filter)

#process.mahiDebugger.recoLabel = cms.InputTag('hbhereco')

process.ntuplizer_step = cms.Path(process.filter*process.analysis)
#process.ntuplizer_step = cms.Path(process.analysis)
# Schedule definition
#process.flat_step = cms.Path(process.mahiDebugger)

#process.testSequence = cms.Sequence(process.filter * process.raw2digi_step)
#process.schedule = cms.Schedule(process.raw2digi_step,process.reconstruction_step,process.endjob_step,process.RECOoutput_step)
process.schedule = cms.Schedule(process.raw2digi_step,process.hltFilter_step,process.reconstruction_step,process.ntuplizer_step)
#process.schedule = cms.Schedule(process.raw2digi_step,process.reconstruction_step,process.flat_step,process.ntuplizer_step)
from PhysicsTools.PatAlgos.tools.helpers import associatePatAlgosToolsTask
associatePatAlgosToolsTask(process)

#Setup FWK for multithreaded
process.options.numberOfThreads = 4
process.options.numberOfStreams = 0



# Customisation from command line

#Have logErrorHarvester wait for the same EDProducers to finish as those providing data for the OutputModule
from FWCore.Modules.logErrorHarvester_cff import customiseLogErrorHarvesterUsingOutputCommands
process = customiseLogErrorHarvesterUsingOutputCommands(process)

# Add early deletion of temporary data products to reduce peak memory need
from Configuration.StandardSequences.earlyDeleteSettings_cff import customiseEarlyDelete
process = customiseEarlyDelete(process)
# End adding early deletion
