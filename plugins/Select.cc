// -*- C++ -*-
//
// Package:    test/Select
// Class:      Select
//
/**\class Select Select.cc test/Select/plugins/Select.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Anton Stepennov
//         Created:  Tue, 19 Apr 2022 08:52:38 GMT
//
//

// system include files
#include <memory>
#include <TFile.h>
#include "TProfile.h"
#include "TTree.h"
#include <iostream>
#include <algorithm>
#include <sstream>
#include <cmath>

// user include files
#include "DataFormats/Common/interface/TriggerResults.h"
#include "DataFormats/Provenance/interface/Provenance.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/TrackReco/interface/TrackFwd.h"

 using namespace edm;
  using namespace std;

#include "DataFormats/HcalRecHit/interface/HcalRecHitCollections.h"
 #include "DataFormats/HcalRecHit/interface/HcalSourcePositionData.h"
//
// class declaration
//

// If the analyzer does not use TFileService, please remove
// the template argument to the base class so the class inherits
// from  edm::one::EDAnalyzer<>
// This will improve performance in multithreaded jobs.

using reco::TrackCollection;

class Select : public edm::one::EDAnalyzer<edm::one::SharedResources> {
public:
  explicit Select(const edm::ParameterSet&);
  ~Select();

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
  void beginJob() override;
  void analyze(const edm::Event&, const edm::EventSetup&) override;
  void endJob() override;

  // ----------member data ---------------------------
   edm::EDGetTokenT<edm::SortedCollection<HBHERecHit>> tok_hf_;
  edm::EDGetTokenT<TrackCollection> tracksToken_;  //used to select what tracks to read from configuration file
edm::EDGetTokenT<HBHEChannelInfoCollection> ChannelInfosToken_;
edm::EDGetTokenT<TriggerResults> tok_trig_;

    std::vector<float>         RecHit;
    std::vector<int>         iEta;
    std::vector<int>         iPhi;
    std::vector<int>         iDepth;
    std::vector<float>         Time;
    std::vector<float>         Charge;

    std::vector<float>         Charge0;
    std::vector<float>         Charge1;
    std::vector<float>         Charge2;
    std::vector<float>         Charge3;
    std::vector<float>         Charge4;
    std::vector<float>         Charge5;
    std::vector<float>         Charge6;
    std::vector<float>         Charge7;

    std::vector<float>         TDC0;
    std::vector<float>         TDC1;
    std::vector<float>         TDC2;
    std::vector<float>         TDC3;
    std::vector<float>         TDC4;
    std::vector<float>         TDC5;
    std::vector<float>         TDC6;
    std::vector<float>         TDC7;

	bool trig1;
	bool trig2;
	bool trig3;
	bool trig4;

TTree *wztree;
      TFile *f;
std::string fileName;

#ifdef THIS_IS_AN_EVENTSETUP_EXAMPLE
  edm::ESGetToken<SetupData, SetupRecord> setupToken_;
#endif
};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
Select::Select(const edm::ParameterSet& iConfig)
     {
fileName = (iConfig.getParameter<string>("outputFileName"));
//iConfig.getUntrackedParameter<std::string>("fileName","default-filename.root");
tok_hf_  = consumes<edm::SortedCollection<HBHERecHit>>(edm::InputTag("hbhereco")); // for pp "hfreco"
    ChannelInfosToken_ = consumes<HBHEChannelInfoCollection>(edm::InputTag("hbhereco"));
tok_trig_  = consumes<TriggerResults>(edm::InputTag("TriggerResults","","HLT"));



#ifdef THIS_IS_AN_EVENTSETUP_EXAMPLE
  setupDataToken_ = esConsumes<SetupData, SetupRecord>();
#endif
  //now do what ever initialization is needed
}

Select::~Select() {
  // do anything here that needs to be done at desctruction time
  // (e.g. close files, deallocate resources etc.)
  //
  // please remove this method altogether if it would be left empty
}

//
// member functions
//

// ------------ method called for each event  ------------
void Select::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup) {
  using namespace edm;
using namespace std;


 edm::Handle<edm::SortedCollection<HBHERecHit>> hf_rechit;
   iEvent.getByToken(tok_hf_, hf_rechit);

 edm::Handle<HBHEChannelInfoCollection> ChannelInfosHandle;
    iEvent.getByToken(ChannelInfosToken_, ChannelInfosHandle);
    auto ChannelInfos = ChannelInfosHandle.product();

//bool trig = false;

trig1 = false;
trig2 = false;
trig3 = false;
trig4 = false;

edm::Handle<edm::TriggerResults> triggerResults;
//edm::InputTag trigResultsTag("TriggerResults","","HLT");
//iEvent.getByLabel(trigResultsTag,triggerResults);
iEvent.getByToken(tok_trig_, triggerResults);
const edm::TriggerNames & triggerNames = iEvent.triggerNames(*triggerResults);


//cout<<"new event"<<endl;
 for(unsigned i=0; i<triggerNames.size(); i++) {
if(triggerResults->accept(i)) {

if((int)(triggerNames.triggerName(i)).find("HLT_ZeroBias_FirstBXAfterTrain")>-1) trig1 = true;
if((int)(triggerNames.triggerName(i)).find("HLT_ZeroBias_FirstCollisionInTrain_")>-1) trig2 = true;
if((int)(triggerNames.triggerName(i)).find("HLT_ZeroBias_IsolatedBunches_")>-1) trig3 = true;
if((int)(triggerNames.triggerName(i)).find("HLT_ZeroBias_FirstCollisionAfterAbortGap_")>-1) trig4 = true;

// if((int)(triggerNames.triggerName(i)).find("")>-1) std::cout<<"shoot!!  "<<triggerNames.triggerName(i)<<std::endl;;


}
}


//for(edm::SortedCollection<HBHERecHit> hf_iter = hf_rechit->begin(); hf_iter != hf_rechit->end(); ++hf_iter) {
//hf_iter->energy();
//}
//cout<<hf_rechit->size()<<endl;


         RecHit = {};
        iEta = {};
         iPhi = {};
         iDepth = {};
         Time = {};

   TDC0 = {};
        TDC1 = {};
        TDC2 = {};
        TDC3 = {};
        TDC4 = {};
        TDC5 = {};
        TDC6 = {};
        TDC7 = {};

	Charge = {};
	Charge0 = {};
        Charge1 = {};
        Charge2 = {};
        Charge3 = {};
        Charge4 = {};
        Charge5 = {};
        Charge6 = {};
        Charge7 = {};
/*
int counter = 0;
 for (edm::SortedCollection<HBHERecHit>::const_iterator
       hhit = hf_rechit->begin ();
       hhit != hf_rechit->end (); hhit++)
    {
      HcalDetId hcalDetId_rh = hhit->id ();

	auto ChannelInfo = (*ChannelInfos)[counter];
      int ieta = hcalDetId_rh.ieta ();
      int iphi = hcalDetId_rh.iphi ();
      int idepth = hcalDetId_rh.depth ();
	RecHit.push_back(hhit->energy());
	iEta.push_back(ieta);
	iPhi.push_back(iphi);
	iDepth.push_back(idepth);
	Time.push_back(hhit->time());

//cout<<"ieta | iphi | depth: "<<ieta<<" | "<<iphi<<" | "<<depth<<endl;

double	totalCharge = 0;
	for(int ts = 0; ts < 8; ts++)
	{
		totalCharge += (ChannelInfo.tsRawCharge(ts-3)  - ChannelInfo.tsPedestal(ts-3));
		//cout<<"q vs pedestal:   "<<ChannelInfo.tsRawCharge(ts-3)<<" , "<<ChannelInfo.tsPedestal(ts-3)<<endl;
	}

counter++;
Charge.push_back(totalCharge);
//cout<<"total charge:  "<<totalCharge<<endl;


//cout<<"time1 vs time 2 : "<<hcalDetId_rh.time()<<" , "<<hhit->time()<<endl;
}
*/

int nRecHits = hf_rechit->size();
for (int iRecHit = 0; iRecHit < nRecHits; iRecHit++) {
auto HBHERecHit = (*hf_rechit)[iRecHit];
auto ChannelInfo = (*ChannelInfos)[iRecHit];
HcalDetId hcalDetId_rh = HBHERecHit.id ();
    int ieta = hcalDetId_rh.ieta ();
      int iphi = hcalDetId_rh.iphi ();
      int idepth = hcalDetId_rh.depth ();
        RecHit.push_back(HBHERecHit.energy());
        iEta.push_back(ieta);
        iPhi.push_back(iphi);
        iDepth.push_back(idepth);
        Time.push_back(HBHERecHit.time());
	double  totalCharge = 0;
//cout<<"ieta | iphi | depth: "<<ieta<<" | "<<iphi<<" | "<<idepth<<endl;
//cout<<"new rechit: "<<endl;
        for(int ts = 0; ts < 8; ts++)
        {
//	cout<<"TS"<<ts<<" "<<(ChannelInfo.tsRawCharge(ts))<<endl;
//if(ChannelInfo.tsRiseTime(ts) != -100 && ChannelInfo.tsRiseTime(ts) != -110) cout<<"TS"<<ts<<" "<<ChannelInfo.tsRiseTime(ts)<<endl;
// cout<<"q vs pedestal:   "<<ChannelInfo.tsRawCharge(ts)<<" , "<<ChannelInfo.tsPedestal(ts)<<endl;
                totalCharge += (ChannelInfo.tsRawCharge(ts)  - ChannelInfo.tsPedestal(ts));
//		cout<<" ts "<<ts<<" "<<(ChannelInfo.tsRawCharge(ts)  - ChannelInfo.tsPedestal(ts))<<endl;
//		cout<<ts<<" :  rising time:  "<<ChannelInfo.tsRiseTime(ts)<<endl;
        }


//cout<<endl;
Charge0.push_back(ChannelInfo.tsRawCharge(0)  - ChannelInfo.tsPedestal(0));
Charge1.push_back(ChannelInfo.tsRawCharge(1)  - ChannelInfo.tsPedestal(1));
Charge2.push_back(ChannelInfo.tsRawCharge(2)  - ChannelInfo.tsPedestal(2));
Charge3.push_back(ChannelInfo.tsRawCharge(3)  - ChannelInfo.tsPedestal(3));
Charge4.push_back(ChannelInfo.tsRawCharge(4)  - ChannelInfo.tsPedestal(4));
Charge5.push_back(ChannelInfo.tsRawCharge(5)  - ChannelInfo.tsPedestal(5));
Charge6.push_back(ChannelInfo.tsRawCharge(6)  - ChannelInfo.tsPedestal(6));
Charge7.push_back(ChannelInfo.tsRawCharge(7)  - ChannelInfo.tsPedestal(7));

TDC0.push_back(ChannelInfo.tsRiseTime(0));
TDC1.push_back(ChannelInfo.tsRiseTime(1));
TDC2.push_back(ChannelInfo.tsRiseTime(2));
TDC3.push_back(ChannelInfo.tsRiseTime(3));
TDC4.push_back(ChannelInfo.tsRiseTime(4));
TDC5.push_back(ChannelInfo.tsRiseTime(5));
TDC6.push_back(ChannelInfo.tsRiseTime(6));
TDC7.push_back(ChannelInfo.tsRiseTime(7));


Charge.push_back(totalCharge);
//cout<<"depth | ieta | iphi | Q:  "<<idepth<<" | "<<ieta<<" | "<<iphi<<" | "<<totalCharge<<endl;
}

if( trig3 == true)
{
wztree->Fill();
}

#ifdef THIS_IS_AN_EVENTSETUP_EXAMPLE
  // if the SetupData is always needed
  auto setup = iSetup.getData(setupToken_);
  // if need the ESHandle to check if the SetupData was there or not
  auto pSetup = iSetup.getHandle(setupToken_);
#endif
}

// ------------ method called once each job just before starting event loop  ------------
void Select::beginJob() {
  // please remove this method if not needed
using namespace std;
f = new TFile(fileName.c_str(), "RECREATE");	  //// the name of the Root file should be set in Jet.cfg!
wztree  = new TTree("wztree", "tracks tree");


wztree->Branch("trig1", &trig1, "trig1/B");
wztree->Branch("trig2", &trig2, "trig2/B");
wztree->Branch("trig3", &trig3, "trig3/B");
wztree->Branch("trig4", &trig4, "trig4/B");

wztree->Branch("Time", &Time);
wztree->Branch("RecHit", &RecHit);
wztree->Branch("iEta", &iEta);
wztree->Branch("iPhi", &iPhi);
wztree->Branch("iDepth", &iDepth);
wztree->Branch("Charge", &Charge);

wztree->Branch("Charge0", &Charge0);
wztree->Branch("Charge1", &Charge1);
wztree->Branch("Charge2", &Charge2);
wztree->Branch("Charge3", &Charge3);
wztree->Branch("Charge4", &Charge4);
wztree->Branch("Charge5", &Charge5);
wztree->Branch("Charge6", &Charge6);
wztree->Branch("Charge7", &Charge7);

wztree->Branch("TDC0", &TDC0);
wztree->Branch("TDC1", &TDC1);
wztree->Branch("TDC2", &TDC2);
wztree->Branch("TDC3", &TDC3);
wztree->Branch("TDC4", &TDC4);
wztree->Branch("TDC5", &TDC5);
wztree->Branch("TDC6", &TDC6);
wztree->Branch("TDC7", &TDC7);

}

// ------------ method called once each job just after ending the event loop  ------------
void Select::endJob() {
  // please remove this method if not needed
using namespace std;
f->WriteTObject(wztree);
delete wztree;
f->Close();
}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void Select::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);

  //Specify that only 'tracks' is allowed
  //To use, remove the default given above and uncomment below
  //ParameterSetDescription desc;
  //desc.addUntracked<edm::InputTag>("tracks","ctfWithMaterialTracks");
  //descriptions.addWithDefaultLabel(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(Select);
